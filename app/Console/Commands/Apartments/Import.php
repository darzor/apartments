<?php

namespace App\Console\Commands\Apartments;

use App\Models\Apartment;
use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Maatwebsite\Excel\Facades\Excel;

class Import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apartments:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Apartments import';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data_e = Excel::toArray( null,storage_path( 'export.xlsx' ) );
        $data_e = $data_e[0] ?? [];

        foreach ( $data_e as $row ){

            Apartment::create( [
                'name' => $row[0],
                'price' => $row[1],
                'currency' => $row[2],
                'description' => $row[3],
                'properties' => json_decode( $row[4], true ),
                'category_id' => $row[5],
                'rating' => $row[6]
            ] );
        }

        return 0;
    }
}
