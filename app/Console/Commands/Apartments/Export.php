<?php

namespace App\Console\Commands\Apartments;

use App\Models\Apartment;
use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Export extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apartments:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Apartments export';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $apartments = Apartment::all();

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()
            ->setTitle( 'Export' )
            ->setSubject( 'Apartments' )
            ->setCreator(config( 'app.name' ) )
            ->setLastModifiedBy( config('app.name'));

        foreach ( $apartments as $key => $apartment ){
            $spreadsheet->getActiveSheet()->setCellValue( 'A'.($key+1), $apartment->name );
            $spreadsheet->getActiveSheet()->setCellValue( 'B'.($key+1), $apartment->price );
            $spreadsheet->getActiveSheet()->setCellValue( 'C'.($key+1), $apartment->currency );
            $spreadsheet->getActiveSheet()->setCellValue( 'D'.($key+1), $apartment->description );
            $spreadsheet->getActiveSheet()->setCellValue( 'E'.($key+1), json_encode( $apartment->properties ) );
            $spreadsheet->getActiveSheet()->setCellValue( 'F'.($key+1), $apartment->category_id );
            $spreadsheet->getActiveSheet()->setCellValue( 'G'.($key+1), $apartment->rating );
        }

        $writer = IOFactory::createWriter($spreadsheet, "Xlsx");
        $writer->save( storage_path( 'export.xlsx'  ) );

        return 0;
    }
}
