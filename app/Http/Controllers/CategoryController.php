<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index( Request $request )
    {
        $filters = $request->filters;

        $categories = Category::where( function ( $q ) use ( $filters )  {

            if( isset( $filters['id'] ) )
                $q->where( 'id', $filters['id'] );

            if( isset( $filters['parent_id'] ) )
                $q->where( 'parent_id', $filters['parent_id'] );

            if( isset( $filters['name'] ) )
                $q->where( 'name', 'like', '%' . $filters['name'] . '%' );

        });

        $total = $categories->count();
        $page = $request->page ?? 1;
        $per_page = $request->per_page ?? 100;

        $categories->orderBy( $request->order_by ?? 'id', $request->order ?? 'asc' )
            ->offset( $per_page * ( $page -1 ) )
            ->limit( $per_page );


        return $this->sendResponse( [
            'data' => $categories->get(),
            'results' => $total,
            'results_from' => ( ( $page - 1 ) * $per_page ) + 1,
            'results_to' => ( ( $page - 1 ) * $per_page ) + $per_page,
            'pages' => intval($total / $per_page )
        ] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store( CategoryRequest $request)
    {
        $category = Category::create( $request->all() );
        return $this->sendResponse( $category );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $category = Category::find( $id );

        if( $category )
            return $this->sendResponse( $category );
        else
            return $this->sendError( 'Not found' );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CategoryRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $category = Category::find( $id );

        if( $category ){
            $category->update( $request->all() );
            return $this->sendResponse( $category );
        }  else {
            return $this->sendError( 'Not found' );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $category = Category::find( $id );

        if( $category ) {
            $category->delete();
            return $this->sendResponse([]);
        } else {
            return $this->sendError( 'Not found' );
        }
    }
}
