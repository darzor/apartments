<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class LoginController extends BaseController
{

    public function register( RegisterRequest  $request ){

        $input = $request->all();

        $input['password'] = bcrypt($input['password']);
        $input['amount'] = rand( 0, 999999 );
        $user = User::create($input);

        return $this->sendResponse(
            [],
            'User register successfully.'
        );
    }




    public function login( LoginRequest  $request ){

        if(auth()->attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();

            return $this->sendResponse(
                [
                    'user' => auth()->user(),
                    'token' => $user->createToken('usertoken')->accessToken
                ],
                'User login successfully.'
            );
        }
        else{
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }
    }




    public function logout(Request $request )
    {

        if( auth()->user() )
            auth()->user()->token()->revoke();

        return $this->sendResponse(
            [],
            'User logout successfully'
        );
    }
}
