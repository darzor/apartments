<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApartmentRequest;
use App\Http\Requests\RateRequest;
use App\Models\Apartment;
use App\Models\Category;
use App\Models\UserApartment;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApartmentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index( Request $request )
    {


        $filters = $request->filters ?? [];

        $apartments = Apartment::with([
                'category'
            ])
            ->where( function( $q ) use ( $filters ) {

                if( isset( $filters['id'] ) )
                    $q->where( 'id', $filters['id'] );

                if( isset( $filters['name'] ) )
                    $q->where( 'name', 'like', '%' . $filters['name'] . '%' );

                if( isset( $filters['price'] ) )
                    $q->where( 'price', $filters['price'] );

                if( isset( $filters['price_from'] ) )
                    $q->where( 'price', '>=', $filters['price_from'] );

                if( isset( $filters['price_to'] ) )
                    $q->where( 'price', '<=', $filters['price_to'] );

                if( isset( $filters['currency'] ) )
                    $q->where( 'currency', 'like', $filters['currency'] . '%' );

                if( isset( $filters['description'] ) )
                    $q->where( 'description', 'like', '%'. $filters['description'] . '%' );

                if( isset( $filters['category_id'] ) ){
                    if( is_array( $filters['category_id'] ) )
                        $q->whereIn( 'category_id', $filters['category_id'] );
                    else
                        $q->where( 'category_id', $filters['category_id'] );
                }

                if( isset( $filters['rating'] ) ){
                    if( is_array( $filters['rating'] ) )
                        $q->whereIn( 'rating', $filters['rating'] );
                    else
                        $q->where( 'rating', $filters['rating'] );
                }

                if( isset( $filters['slug'] ) )
                    $q->where( 'slug', $filters['slug'] );

                if( isset( $filters['size'] ) )
                    $q->where( 'properties', 'like', '%\"size\":' . $filters['size'] . '%' );

            } )
            ->whereHas( 'category', function( $q ) use ( $filters ) {

                if( isset( $filters['category_parent_id'] ) ){
                    if( is_array( $filters['category_parent_id'] ) )
                        $q->whereIn( 'parent_id', $filters['category_parent_id'] );
                    else
                        $q->where( 'parent_id', $filters['category_parent_id'] );
                }

                if( isset( $filters['category_name'] ) )
                    $q->where( 'name', 'like', '%' . $filters['category_name' ] . '%' );
            } );


        $total = $apartments->count();
        $page = $request->page ?? 1;
        $per_page = $request->per_page ?? 100;

        $order_by_columns = $request->order_by ?? [];

        if( count( $order_by_columns ) )
        foreach ( $order_by_columns as $column ){
            $apartments = $apartments->orderBy(
                $column['column'] ?? 'id',
                $column['order'] && in_array( $column['order'], ['asc', 'desc'] ) ? $column['order'] : 'asc'
            );
        } else {
            $apartments = $apartments->orderBy( 'id', 'asc' );
        }

        $apartments = $apartments->offset( $per_page * ( $page -1 ) );
        $apartments = $apartments->limit( $per_page );



        $options = $request->options ?? [];
        if( isset( $options['currency'] ) ){

            $currencies = Apartment::select( 'currency' )
                ->groupBy( 'currency' )
                ->get()
                ->pluck('currency')
                ->toArray();

            $apartments = $apartments->get();

            $client = new Client();
            try {
                $response = $client->request(
                    'GET',
                    'http://data.fixer.io/api/latest?access_key=' . config('app.fixer_key') .'&base='. $options['currency'] .'&symbols='. implode( ',' , $currencies )
                );
            } catch ( BadResponseException $exception ){
                //
            }

            $response = json_decode( $response->getBody()->getContents(), true ) ?? [];

            $apartments = $apartments->map( function ( $item ) use ( $options, $response ) {

                if(
                    isset( $response['rates'] )
                    && isset( $response['rates'][ $options['currency'] ] )
                ) {
                    $item->price = number_format ($item->price / $response['rates'][ $item->currency ], '2', '.', '' );
                    $item->currency = $options['currency'];
                }

                return $item;

            } );
        } else {
            $apartments = $apartments->get();
        }

        //$client = new Client();
        //$response = $client->request( 'GET', 'http://data.fixer.io/api/latest?access_key=11873d2bdb60accee0247650eeb79749&base=EUR&symbols=USD');

        //dd( json_decode( $response->getBody()->getContents(), true ) );


        return $this->sendResponse( [
            'data' => $apartments,
            'results' => $total,
            'results_from' => ( ( $page - 1 ) * $per_page ) + 1,
            'results_to' => ( ( $page - 1 ) * $per_page ) + $per_page,
            'pages' => intval($total / $per_page )
        ] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ApartmentRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ApartmentRequest $request)
    {
        $apartment = Apartment::create( $request->all() );
        return $this->sendResponse( $apartment );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $apartment = Apartment::find( $id );

        if( $apartment )
            return $this->sendResponse( $apartment );
        else
            return $this->sendError( 'Not found' );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ApartmentRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ApartmentRequest $request, $id)
    {
        $apartment = Apartment::find( $id );

        if( $apartment ){
            $apartment->update( $request->all() );
            return $this->sendResponse( $apartment );
        }  else {
            return $this->sendError( 'Not found' );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $apartment = Apartment::find( $id );

        if( $apartment ) {
            $apartment->delete();
            return $this->sendResponse([]);
        } else {
            return $this->sendError( 'Not found' );
        }
    }


    public function rate( RateRequest $request ){

        $user = auth()->user();

        $user_apartment = $user->user_apartment->where( 'apartment_id', $request->apartment_id )->first();

        if( $user_apartment ){
            return $this->sendResponse( [], 'You have already rated this apartment' );
        } else {

            DB::beginTransaction();

            UserApartment::create([
                'user_id' => $user->id,
                'apartment_id' => $request->apartment_id,
                'rating' => $request->rating
            ]);

            $avg = UserApartment::where( 'apartment_id', $request->apartment_id )
                ->avg( 'rating' );

            Apartment::find( $request->apartment_id )->update( ['user_rating' => $avg] );

            DB::commit();

            return $this->sendResponse( [], 'You have successfully rated' );
        }
    }
}
