<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'price' => 'required|numeric|between:-9999999999.99,9999999999.99',
            'currency' => 'required|string|min:3|max:3',
            'description' => 'nullable|string',
            'properties' => 'nullable|array',
            'category_id' => 'required|integer|exists:categories,id',
            'rating' => 'required|integer|between:0,5'
        ];
    }
}
