<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Apartment extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'apartments';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public $fillable = [];
    public $guarded = [];
    public $hidden = [];

    public function category(){
        return $this->belongsTo( Category::class, 'category_id' );
    }

    public function user_apartment(){
        return $this->hasMany( UserApartment::class, 'user_apartment_id' );
    }

    public function setNameAttribute( $name ){

        $this->attributes['name'] = $name;

        $name = str_replace( ' ', '_', $name );
        $name = iconv('UTF-8', 'ASCII//TRANSLIT', $name );
        $name = preg_replace('/[^A-Za-z0-9_.]/', '', $name );

        $this->attributes['slug'] = strtolower( $name );
    }

    public function getPropertiesAttribute(){
        return json_decode( $this->attributes['properties'] , true );
    }

    public function setPropertiesAttribute( $data ){
        $this->attributes['properties'] = json_encode( is_array( $data ) ? $data : [] );
    }
}
