<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserApartment extends Model
{
    use HasFactory;

    protected $table = 'user_apartment';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public $fillable = [];
    public $guarded = [];
    public $hidden = [];


    public function user(){
        return $this->belongsTo( User::class, 'user_id' );
    }

    public function apartment(){
        return $this->belongsTo( Apartment::class, 'apartment_id' );
    }
}
