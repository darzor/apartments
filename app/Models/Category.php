<?php

namespace App\Models;

use Database\Seeders\CategorySeeder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'categories';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public $fillable = [];
    public $guarded = [];
    public $hidden = [];


    public function apartment(){
        return $this->hasMany( Apartment::class, 'category_id' );
    }

    public function parent(){
        return $this->belongsTo( CategorySeeder::class, 'parent_id' );
    }

    public function children(){
        return $this->hasMany( CategorySeeder::class, 'parent_id' );
    }
}
