<?php

namespace Database\Seeders;

use App\Models\Apartment;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ApartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = [ 'HRK', 'EUR', 'USD' ];
        $faker = Factory::create();

        for( $i = 1; $i <= 100; $i++ ) {
            Apartment::create([
                'name' => 'Apartment ' . $i,
                'price' => rand( 1000, 10000000 ) / 100,
                'currency' => $currencies[ rand( 0, 2 ) ],
                'description' => file_get_contents('http://loripsum.net/api'),
                'properties' => [
                    'size' => rand( 10, 100 )
                ],
                'category_id' => rand( 1,10 ),
                'rating' => rand( 1,5 )
            ]);
        }
    }
}
