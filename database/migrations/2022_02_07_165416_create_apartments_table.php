<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->string( 'name' );
            $table->decimal( 'price', 10, 2 );
            $table->string( 'currency', 3 );
            $table->text( 'description' )->nullable();
            $table->json( 'properties' )->nullable();
            $table->unsignedBigInteger( 'category_id' );
            $table->foreign( 'category_id' )->references( 'id' )->on( 'categories' );
            $table->tinyInteger( 'rating' )->default( 0 );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartments');
    }
}
